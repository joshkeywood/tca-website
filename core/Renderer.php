<?php
/**
 * User: Hunter
 * Date: 2/27/14
 * Time: 1:43 PM
 */

class Renderer {

    public static function PageExists($pagename) {
        return file_exists(self::filenameFromPagename($pagename));
    }

    protected static function filenameFromPagename($pagename) {
        $templatePageDir = self::getTemplatePageDir();
        return "$templatePageDir/{$pagename}.twig";
    }

    protected static function relFilenameFromPagename($pagename) {
        $rel = self::getTemplateRelDir();
        return "$rel/{$pagename}.twig";
    }

    public static function getTemplateDir() {
        return dirname(__FILE__).'/../templates';
    }

    public static function getTemplateRelDir() {
        return 'pages';
    }

    public static function getTemplatePageDir() {
        return self::getTemplateDir().'/'.self::getTemplateRelDir();
    }

    public static function finalRender($pagename, $options = array()) {
        $loader = new Twig_Loader_Filesystem(self::getTemplateDir());
        $twig = new Twig_Environment($loader, array());
        $html = $twig->render(
            self::relFilenameFromPagename($pagename),
            array_merge(self::getDefaultParameters(), $options)
        );
        die($html);
    }

    protected static function getDefaultParameters() {
        return array(
            "master" => "optimized.twig",
            "requestUri" => $_SERVER["REQUEST_URI"],
            "files_css" => array(
                "normal" => "normalize.css",
                "main" => "main.css",
                "tca" => "tca.css",
                "mobile" => "mobile.css",
            ),
            "files_js" => array(
                "plugins" => "plugins.js",
                "main" => "main.js",
                "analytics" => "analytics.js",
            ),
        );
    }
} 