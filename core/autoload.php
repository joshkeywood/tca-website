<?php
/**
 * Created by PhpStorm.
 * User: hunter
 * Date: 2/19/14
 * Time: 2:29 PM
 */

require_once(dirname(__FILE__).'/Renderer.php');

function tcaw_autoload_cb($className) {
    $cleaned = str_replace('_', '/', $className);
    $path = dirname(__FILE__)."/$cleaned.php";
    if (file_exists($path)) {
        require_once($path);
    }
}

spl_autoload_register('tcaw_autoload_cb');

function wautoload_vendor_libs($className) {
    $parts = explode('_', $className);
    $libs = array(
        "Twig" => "vendor/twig/twig/lib"
    );

    if (array_key_exists($parts[0], $libs)) {
        $fname = implode('/', $parts);
        $libpath = $libs[$parts[0]];
        $base = dirname(__FILE__).'/..';

        $path = "$base/$libpath/$fname.php";
        if (file_exists($path)) {
            require_once($path);
        }
    }
}

spl_autoload_register('wautoload_vendor_libs');