<?php
/**
 * Created by PhpStorm.
 * User: hunter
 * Date: 2/14/14
 * Time: 10:34 PM
 */
require_once(dirname(__FILE__).'/core/autoload.php');

$requestedPage = isset($_GET['page']) ? $_GET['page'] : "index";
$pageName = null;

if(preg_match("~[^a-zA-Z0-9\\-]~", $requestedPage)) {
    $pageName = "404";
}

if(!Renderer::PageExists($requestedPage)) {
    $pageName = "404";
} else {
    $pageName = $requestedPage;
}

Renderer::finalRender($pageName);