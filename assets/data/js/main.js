
$(document).ready(function(){
    var sliders =  $('.bxslider');
    if (sliders.length > 0) {
        sliders.bxSlider({
            auto: true,
            autoControls: false,
            pause: 7000,
            autoHover: true
        });
    }
});