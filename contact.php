<?php
/**
 * User: Hunter
 * Date: 2/27/14
 * Time: 1:43 PM
 */
require_once(dirname(__FILE__).'/core/autoload.php');
require_once(dirname(__FILE__).'/lib/recaptcha-php-1.11/recaptchalib.php');

$pubKey = "6LdyM-8SAAAAAMZz9XDTYQ3EphQRd4HuYl9ifF_w";
$privKey = "6LdyM-8SAAAAAI-hYoIIFxGvqEQuafaKalbLCOOP";

$options = array("form" => $_POST);

$captchaError = NULL;

if ($_POST["recaptcha_response_field"]) {
    $resp = recaptcha_check_answer ($privKey,
        $_SERVER["REMOTE_ADDR"],
        $_POST["recaptcha_challenge_field"],
        $_POST["recaptcha_response_field"]);

    if ($resp->is_valid) {
        $emails = array(
            "general" => "<Jeremy.mast@tricountyambulanceservice.com>, <Al.peery@tricountyambulanceservice.com>, <josh@kigwired.com>",
            "jeremy" => "<Jeremy.mast@tricountyambulanceservice.com>",
        );
        $to = $emails[$_POST['to']];
        $subj = "{$_POST['subject']} (from: {$_POST['email']})";
        mail($to, $subj, $_POST['message']);
        $options['done'] = true;
    } else {
        $captchaError = $resp->error;
    }
}

$options['captchaHtml'] = recaptcha_get_html($pubKey, $captchaError);
$options['captchaError'] = $captchaError;
Renderer::finalRender("contact", $options);